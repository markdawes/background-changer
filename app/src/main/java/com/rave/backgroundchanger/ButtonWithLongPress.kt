package com.rave.backgroundchanger

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ButtonWithLongPress(
    text: String,
    onClick: ()-> Unit,
    onLongClick: ()-> Unit
){
    Text(
        text = text,
        fontSize = 25.sp,
        color = Color.White,
        modifier = Modifier
            .padding(20.dp)
            .clip(RoundedCornerShape(30.dp))
            .combinedClickable(
                onClick = { onClick() },
                onLongClick = { onLongClick() },
            )
            .background(Color(0xFF4E8098))
            .padding(20.dp)
    )
}

@Preview(showBackground = true)
@Composable
fun LongPressButtonPreview() {
    ButtonWithLongPress(text = "Tap", onClick = { }, onLongClick = { })
}

package com.rave.backgroundchanger

import android.graphics.Color.rgb
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import kotlin.random.Random
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {
    private val _color1 = MutableStateFlow(255)
    val color1: StateFlow<Int> get() = _color1
    private val _color2 = MutableStateFlow(255)
    val color2: StateFlow<Int> get() = _color2
    private val _color3 = MutableStateFlow(255)
    val color3: StateFlow<Int> get() = _color3

    fun changeColor(){
        _color1.value = Random.nextInt(0, 255)
        _color2.value = Random.nextInt(0, 255)
        _color3.value = Random.nextInt(0, 255)
    }

    fun reset(){
        _color1.value = 255
        _color2.value = 255
        _color3.value = 255
    }
}
package com.rave.backgroundchanger

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun BackgroundChangerScreen(changeColor: ()->Unit,
                            reset: ()->Unit,
                            col1: Int, col2: Int, col3: Int) {
    var backgroundColor = android.graphics.Color.rgb(col1, col2, col3)
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .background(Color(backgroundColor)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        ButtonWithLongPress(
            text = "Change Color",
            onClick = { changeColor() },
            onLongClick = { reset() }
        )

    }
}
package com.rave.backgroundchanger

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class MainViewModelTest {

    private val mainViewModel = MainViewModel()

    @Test
    @DisplayName("Test reset functionality")
    fun testIncrement() {
        // Given

        // When
        mainViewModel.changeColor()
        mainViewModel.changeColor()
        mainViewModel.reset()

        // Then
        Assertions.assertEquals(listOf(255, 255, 255), listOf(mainViewModel.color1.value, mainViewModel.color2.value, mainViewModel.color3.value))
    }

}